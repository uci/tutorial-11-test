# Tutorial 11 - Continuous integration dan deployment

> Peringatan: tutorial 11 bersifat **opsional**.

Halooo, selamat datang di tutorial 11 kuliah Game Development. Pada tutorial kali ini,
kamu akan mempelajari cara untuk membuat _CI_ dan _CD_ untuk mengautomisasi integrasi
dan deployment dari proyek Godot kalian. Jadi kita sebagai tim pengembang tidak lagi perlu
untuk secara manual mengexport proyek Godot yang sedang dikerjakan menjadi sebuah executeable
file. Kita juga bisa mengefisiensikan waktu untuk pengupload file executeable tersebut ke sebuah 
marketplace seperti itch.io. 

## Pengantar
Sebagaimana yang kita ketahui, sebagai sebuah tim pengembang kita akan sangat terbantu dengan keberadaan
_CI_ dan _CD_ ini. Metode ini memungkinkan proyek yang kita kerjakan bisa dengan otomatis terintegrasi
dengan proyek yang saat ini sudah ada. Terlebih lagi metode ini bisa mengautomisasi deployment saat kita melakukan 
merge ke suatu branch production, yang biasanya akan terjadi sebuah deployment ke sebuah environtment baru. Untuk 
mengetahui lebih lanjut mengenai apa itu _CI_ dan _CD_ kalian bisa mengakses [link ini](https://en.wikipedia.org/wiki/CI/CD).

Sebenarnya banyak cara untuk membuat _CI_ dan _CD_ pada proyek Godot kalian. Pada tutorial kali ini kita akan membuat Gitlab CI yang
 akan menggunakan sebuah image docker yang dokumentasi lengkapnya dapat diakses pada link 
 [gitlab ini](https://gitlab.com/BARICHELLO/godot-ci). Lalu untuk cara export proyek Godot kalian menggunakan 
 _Command Line_ bisa diakses pada dokumentasi 
[Godot ini](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_projects.html#exporting-from-the-command-line).

## Auto build to Gitlab artifact
Pertama adalah bagaimana kita membuild atau mengexport otomatis proyek kita kedalam executeable berbagai jenis sistem operasi seperti
Windows, MacOS, Ubuntu, atau bahkan berbasis web (HTML5). Berikut adalah langkah-langkah yang bisa kalian ikuti (langkah ini mengacu pada
referensi yang telah diberikan sebelumnya).

1. Copy dan Paste tutorial yang ingin kalian dibuatkan CInya dengan struktur sebagai berikut: 

```
tutorial-11
---- tutorial-X
-------- Assets
-------- Scenes
-------- project.godot
-------- etc.
```

2. Buat file `.gitlab-ci.yml` baru didalam forlader *tutorial-11* dimana setara dengan folder *tutorial-X*, lalu isi dengan script berikut: 

```
image: barichello/godot-ci:3.2.3

stages:
  - export
  - deploy

variables:
  EXPORT_NAME: tutorial-X

windows:
  stage: export
  script:
    - mkdir -v -p build/windows
    - cd $EXPORT_NAME
    - godot -v --export "Windows Desktop" ../build/windows/$EXPORT_NAME.exe
  artifacts:
    name: $EXPORT_NAME-$CI_JOB_NAME
    paths:
      - build/windows

web:
  stage: export
  script:
    - mkdir -v -p build/web
    - cd $EXPORT_NAME
    - godot -v --export "HTML5" ../build/web/index.html
  artifacts:
    name: $EXPORT_NAME-$CI_JOB_NAME
    paths:
      - build/web
```

Secara singkat, script ini akan menjalakan sebuah stage `export` dimana ada dua tahap yaitu windows dan web. Ditiap tahap ini akan membuat sebuah direktori atau filder artifact baru, dimana tiap folder ini akan berisi archive dari export project Godot kalian. 

3. Sesuaikan variabel `EXPORT_NAME: tutorial-X` dengan nama folder project yang tadi kalian copy dan paste ke *tutorial-11*.
4. Buka aplikasi Godot, lalu import project pada *tutorial-X* ini. Lalu setting project export di *Project>export..>Add.. (tambahkan windows dan html sesuai dengan stage: export pada gitlab ci yml tadi)*.

![Add Template](https://docs.godotengine.org/en/stable/_images/export_preset.png)

5. Jika ada warning seperti ini

![Warning](https://docs.godotengine.org/en/stable/_images/export_error.png)

Lakukan instalasi template terlebih dahulu dengan menekan *Manage Export Template > Download (download missing template) > Klik template yang tersedia* lalu tunggu sampai instalasi selesai. 

6. Setelah selesai dan sudah ada dua template windows dan html, lalu tekan close pada pop up export.. lalu save project dengan ctrl+s. Seharusnya akan muncul sebuah file `export_presets.cfg` didalam folder *tutorial-X*.
7. Lakukan push ke Gitlab kalian dan perhatikan pipeline dari commit kalian saat itu. 
8. Jika sudah slesai pipelinenya, cara menddownload artifact yang sudah di build bisa diakses pada *CI/CD gitlab > pipelines > lalu cek button download pada commit yang tadi dilakukan* seharusnya ada 4 pilihan artifact untuk di download.

![Commit Pipeline](images/commit_pipeline.png)

9. Download salah satu artifact lalu extract dan jalankan executable filenya, walaaaa selesai kalian berhasil mengautomisasi build proyek Godot kalian.


## Auto deploy to Gitlab.io
Selanjutnya adalah bagaimana artifact tersebut bisa diakses oleh pengguna pada marketplace atau bisa langsung dimainkan dengan executeable web (HTML5)? Berikut adalah tutorial untuk menjalankan executeable HTML5 tadi dengan menggunakan gilab.io. 

1. Buat sebuah branch baru yang akan menjadi tempat artifact HTML5 dijalankan. Pada contoh ini nama branch adalah `git-deploy`.
2. Buat sebuah folder *public* didalam branch `git-deploy`. Ingat Gitlab tidak bisa mengidentifikasi folder kosong, jadi tambahkan saja sebuah `README.md` bebas didalam folder *public* ini. 
3. Setelah branch baru selesai dibuat, tambahkan potongan script berikut ini di lokasi terbawah file `.gitlab-ci.yml`: 

```
pages:
  stage: deploy
  dependencies:
    - web
  script:
    - git fetch
    - git checkout git-deploy
    - rm -f *.md
    - mv build/web/** ./public
  artifacts:
    paths:
      - public
``` 

Secara singkat script ini mengubah branch ke `git-deploy` dan menghapus semua `.md` lalu akan memindahkan artifact dari stage export web (HTML5) kedalam folder *public*. 

4. Selanjutnya seperti biasa yaitu push hasil pekerjaan kedalam Gitlab. Lalu perhatikan pipeline dari commit ini. 
5. Setelah pipeline berhasil, ketik *https://[user_gitlab_kalian].gitlab.io/tutorial-11/* pada browser kalian. Berikut adalah hasilnya (menggunakan tutorial-3). 

![Web Preview](images/web_preview.jpg)


## Tugas

- [ ] Buat build otomatis kedalam sistem operasi Linux dan MacOS
- [ ] Pastikan archive dari artifact dapat didownload dan berisi file yang diinginkan
- [ ] Buat deployment otomatis kedalam Github
- [ ] Pastikan game dapat dimainkan pada browser dengan mengakses url tertentu
- [ ] Laporkan bagaimana cara mu mengerjakan tugas ini dan lampirkan pula link Github dan URL game browser kalian. Berkas laporan yang dituliskan dalam format Markdown (misal: `T11_[NPM].md`).

## Pengumpulan

Kumpulkan dengan memasukkan berkasnya ke dalam Git dan _push_ ke _fork_ materi
tutorial ini di repositori milik pribadi. **Jangan _push_ atau membuat Merge
Request ke repositori _upstream_ materi tutorial kecuali jika kamu ingin
kontribusi materi atau memperbaiki materi yang sudah dipublikasikan!**

Tenggat waktu pengumpulan adalah **Sabtu, 2 Januari 2021, pukul 21:00**.

## Referensi

- [Gitlab godot-ci](https://gitlab.com/BARICHELLO/godot-ci)
- [Dokumentasi Godot](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_projects.html#exporting-from-the-command-line)
- Materi tutorial pengenalan Godot Engine, kuliah Game Development semester
  gasal 2020/2021 Fakultas Ilmu Komputer Universitas Indonesia.